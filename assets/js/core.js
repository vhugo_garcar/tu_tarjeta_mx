$(document).ready(function(){
    $('.benefits').slick({
      slidesToShow: 3,
      responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          slidesToShow: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: true,
          slidesToShow: 1
        }
      }
      ]
    });
  });