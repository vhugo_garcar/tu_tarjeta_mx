'use strict';
angular
  .module('app', [])
  .directive('limitTo', [
    function() {
      return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
          var limit = parseInt(attrs.limitTo);
          angular.element(elem).on('keypress', function(e) {
            var key;
            if (e.which == null) {
              // IE
              key = e.keyCode;
            }
            if (e.which != 0) {
              // all but IE
              key = e.which;
            }
            if (
              this.value.length == limit &&
              (key != 8 && key !== 46 && key !== undefined)
            ) {
              e.preventDefault();
            }
          });
        }
      };
    }
  ])
  .controller('home', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;

      console.log('TuTarjetaMx home');
      sessionStorage.clear();

      $scope.setOrigin = function(origin) {
        sessionStorage.setItem('origin', origin);
      };
    }
  ])
  .controller('step1', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;

      console.log('TuTarjetaMx step1');

      $scope.onlyNumbers = /^\d+$/;
      $scope.emailExpression = /^\w+([\.\+-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;

      $scope.f = {
        email: '',
        phone: '',
        termsConditions: null,
        privacyNotice: null,
        origin: sessionStorage.getItem('origin')
      };

      $scope.register = function(isValid) {
        // Formulario válido
        if (isValid) {
          if (sessionStorage.getItem('leadId')) {
            $http
              .patch('/lead/' + sessionStorage.getItem('leadId'), $scope.f)
              .then(
                res => {
                  $scope.f['lead'] = sessionStorage.getItem('leadId');
                  if (sessionStorage.getItem('applicationId')) {
                    $http
                      .patch(
                        '/application/' +
                        sessionStorage.getItem('applicationId'),
                        $scope.f
                      )
                      .then(
                        res => {
                          window.location = '../step2';
                        },
                        err => {
                          console.error(err);
                        }
                      );
                  } else {
                    $http.post('/application', $scope.f).then(
                      res => {
                        sessionStorage.setItem('applicationId', res.data.id);
                        window.location = '../step2';
                      },
                      err => {
                        console.error(err);
                      }
                    );
                  }
                },
                err => {
                  console.error(err);
                }
              );
          } else {
            $http.post('/lead', $scope.f).then(
              res => {
                $scope.f['lead'] = res.data.id;
                sessionStorage.setItem('leadId', res.data.id);
                if (sessionStorage.getItem('applicationId')) {
                  $http
                    .patch(
                      '/application/' + sessionStorage.getItem('applicationId'),
                      $scope.f
                    )
                    .then(
                      res => {
                        window.location = '../step2';
                      },
                      err => {
                        console.error(err);
                      }
                    );
                } else {
                  $http.post('/application', $scope.f).then(
                    res => {
                      sessionStorage.setItem('applicationId', res.data.id);
                      window.location = '../step2';
                    },
                    err => {
                      console.error(err);
                    }
                  );
                }
              },
              err => {
                console.error(err);
              }
            );
          }
        } else {
          // Formulario no válido
          console.log('No es válido');
        }
      };
    }
  ])
  .controller('step2', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;

      console.log('TuTarjetaMx step2');
      $scope.curpExpression = /[A-Z][A,E,I,O,U,X][A-Z]{2}[0-9]{2}[0-1][0-9][0-3][0-9][M,H][A-Z]{2}[B,C,D,F,G,H,J,K,L,M,N,Ñ,P,Q,R,S,T,V,W,X,Y,Z]{3}/;
      $scope.curpHomoclave = /^[0-9,A-Z][0-9]$/;
      $scope.lettersForNames = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ-\s]+$/;

      if (
        !sessionStorage.getItem('leadId') ||
        !sessionStorage.getItem('applicationId')
      ) {
        window.location = '../step1';
      }

      var date = new Date();
      date.setFullYear(date.getFullYear() - 18);
      date.setMonth(0);
      date.setDate(1);

      $scope.months = [
        'ENE',
        'FEB',
        'MAR',
        'ABR',
        'MAY',
        'JUN',
        'JUL',
        'AGO',
        'SEP',
        'OCT',
        'NOV',
        'DIC'
      ];

      $scope.f = {
        name: '',
        paternalSurname: '',
        maternalSurname: '',
        dayBirth: 1,
        monthBirth: 'ENE',
        yearBirth: date.getFullYear(),
        state: '',
        civilState: '',
        gender: '',
        curp: '',
        homoclave: ''
      };

      $scope.daysInMonth = function() {
        var month = $scope.months.indexOf($scope.f.monthBirth) + 1;
        $scope.days = $scope.range(
          1,
          new Date($scope.f.yearBirth, month, 0).getDate(),
          1
        );
        console.log($scope.f);
        $scope.generateCURP();
      };

      $scope.generateCURP = function() {
        const consonants = new RegExp(/[bcdfghjklmnñpqrstvwxyz]/i);
        const vocals = new RegExp(/[aeiou]/i);
        const month =
          $scope.months.findIndex(el => el === $scope.f.monthBirth) + 1;
        let chunk1;
        let chunk2;
        let chunk3;
        let chunk4;
        let chunk10;
        let chunk11;
        let chunk12;
        chunk1 = chunk2 = chunk3 = chunk4 = chunk10 = chunk11 = chunk12 = '';
        if ($scope.f.paternalSurname) {
          const vocalsMatch = $scope.f.paternalSurname.match(vocals);
          chunk1 = $scope.f.paternalSurname[0];
          chunk2 = vocalsMatch ? vocalsMatch[0] : '';
          if ($scope.f.paternalSurname.length > 1) {
            const aux = $scope.f.paternalSurname.substring(1).match(consonants);
            chunk10 = aux ? aux[0] : 'X';
          }
        }
        if ($scope.f.maternalSurname) {
          chunk3 = $scope.f.maternalSurname[0];
          if ($scope.f.maternalSurname.length > 1) {
            const aux = $scope.f.maternalSurname.substring(1).match(consonants);
            chunk11 = aux ? aux[0] : 'X';
          }
        }
        if ($scope.f.name) {
          chunk4 = $scope.f.name[0];
          if ($scope.f.name.length > 1) {
            const aux = $scope.f.name.substring(1).match(consonants);
            chunk12 = aux ? aux[0] : 'X';
          }
        }
        const chunk5 = $scope.f.yearBirth.toString().substring(2);
        const chunk6 = month < 10 ? `0${month}` : month;
        const chunk7 =
          $scope.f.dayBirth < 10 ? `0${$scope.f.dayBirth}` : $scope.f.dayBirth;
        const chunk8 =
          $scope.f.gender === '1' ? 'H' : $scope.f.gender === '2' ? 'M' : '';
        const chunk9 = $scope.state ?
          $scope.state.find(el => el.id === $scope.f.state).code :
          '';
        $scope.f.curp = `${chunk1}${chunk2}${chunk3}${chunk4}${chunk5}${chunk6}${chunk7}${chunk8}${chunk9}${chunk10}${chunk11}${chunk12}`.toUpperCase();
      };

      $scope.range = function(min, max, step) {
        step = Math.abs(step) || 1;
        var input = [];
        var range = Math.abs(max - min) + 1;
        for (var i = 0; i < range; i++) {
          input.push(min);
          min = max - min >= 0 ? min + step : min - step;
        }
        return input;
      };

      $scope.years = $scope.range(
        date.getFullYear(),
        date.getFullYear() - 42,
        1
      );
      $scope.daysInMonth();

      $http.get('/state?sort=description').then(
        res => {
          $scope.state = res.data;
          $scope.f.state = $scope.state[0].id;
        },
        err => {
          // Sweetalert
          console.error(err);
        }
      );

      $http.get('/civilState').then(
        res => {
          $scope.civilState = res.data;
          $scope.f.civilState = $scope.civilState[0].id;
        },
        err => {
          // Sweetalert
          console.error(err);
        }
      );
      $http.get('/gender?sort=id').then(
        res => {
          $scope.gender = res.data;
        },
        err => {
          // Sweetalert
          console.error(err);
        }
      );

      $scope.register = function(isValid) {
        // Formulario válido
        if (isValid) {
          let homoclave = $scope.f.homoclave;
          let rest = Object.assign({}, $scope.f);
          delete rest.homoclave;
          if (homoclave) {
            rest.curp = rest.curp + homoclave;
          }

          sessionStorage.setItem('yearBirth', $scope.f.yearBirth);
          sessionStorage.setItem('stateId', $scope.f.state);

          $http.patch('/lead/' + sessionStorage.getItem('leadId'), rest).then(
            res => {
              $http
                .patch(
                  '/application/' + sessionStorage.getItem('applicationId'),
                  rest
                )
                .then(
                  res => {
                    window.location = '../step3';
                  },
                  err => {
                    console.error(err);
                  }
                );
            },
            err => {
              console.error(err);
            }
          );
        } else {
          // Formulario no válido
          console.log('No es válido');
        }
      };
    }
  ])
  .controller('step3', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;

      console.log('TuTarjetaMx step3');

      if (
        !sessionStorage.getItem('leadId') ||
        !sessionStorage.getItem('applicationId')
      ) {
        window.location = '../step1';
      }

      $scope.f = {
        income: ''
        // checkIncome: '',
      };

      $http.get('/income?sort=id').then(
        res => {
          $scope.income = res.data;
        },
        err => {
          console.error(err);
        }
      );

      $scope.register = function(isValid) {
        console.log('register');
        // Formulario válido
        if (isValid) {
          sessionStorage.setItem('incomeId', $scope.f.income);
          $http
            .patch(
              '/application/' + sessionStorage.getItem('applicationId'),
              $scope.f
            )
            .then(
              res => {
                window.location = '../step4';
              },
              err => {
                console.error(err);
              }
            );
        } else {
          // Formulario no válido
          console.log('No es válido');
        }
      };
    }
  ])
  .controller('step4', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;

      console.log('TuTarjetaMx step4');

      if (
        !sessionStorage.getItem('leadId') ||
        !sessionStorage.getItem('applicationId')
      ) {
        window.location = '../step1';
      }

      $scope.f = {
        profession: ''
      };

      $http.get('/profession?sort=id').then(
        res => {
          $scope.profession = res.data;
        },
        err => {
          // Sweetalert
          console.error(err);
        }
      );

      $scope.register = function(isValid) {
        // Formulario válido
        if (isValid) {
          sessionStorage.setItem('professionId', $scope.f.profession);
          $http
            .patch(
              '/application/' + sessionStorage.getItem('applicationId'),
              $scope.f
            )
            .then(
              res => {
                window.location = '../step5';
              },
              err => {
                console.error(err);
              }
            );
        } else {
          // Formulario no válido
          console.log('No es válido');
        }
      };
    }
  ])
  .controller('step5', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;

      console.log('TuTarjetaMx step5');

      if (
        !sessionStorage.getItem('leadId') ||
        !sessionStorage.getItem('applicationId')
      ) {
        window.location = '../step1';
      }

      $scope.f = {
        debit: ''
      };

      $scope.register = function(isValid) {
        // Formulario válido
        if (isValid) {
          sessionStorage.setItem('debit', $scope.f.debit);
          $http
            .patch(
              '/application/' + sessionStorage.getItem('applicationId'),
              $scope.f
            )
            .then(
              res => {
                window.location = '../step6';
              },
              err => {
                console.error(err);
              }
            );
        } else {
          // Formulario no válido
          console.log('No es válido');
        }
      };
    }
  ])
  .controller('step6', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;

      console.log('TuTarjetaMx step6');

      if (
        !sessionStorage.getItem('leadId') ||
        !sessionStorage.getItem('applicationId')
      ) {
        window.location = '../step1';
      }

      $scope.f = {
        paymentUpToDate: null,
        financialProduct: []
      };

      $http.get('/financialProduct?sort=id').then(
        res => {
          $scope.financialProduct = res.data;
        },
        err => {
          // Sweetalert
          console.error(err);
        }
      );

      $scope.change = function(id) {
        var index = $scope.f.financialProduct.indexOf(id);
        if (index >= 0) {
          $scope.f.financialProduct.splice(index, 1);
        } else {
          $scope.f.financialProduct.push(id);
        }
        $scope.f.paymentUpToDate =
          $scope.f.financialProduct.length > 0 ?
          $scope.f.paymentUpToDate :
          null;
      };

      $scope.register = function(isValid) {
        // Formulario válido
        if (isValid) {
          sessionStorage.setItem('paymentUpToDate', $scope.f.paymentUpToDate);
          $http
            .patch(
              '/application/' + sessionStorage.getItem('applicationId'),
              $scope.f
            )
            .then(
              res => {
                fbq('track', 'Lead');
                window.location = '../results';
              },
              err => {
                console.error(err);
              }
            );
        } else {
          // Formulario no válido
          console.log('No es válido');
        }
      };
    }
  ])
  .controller('email', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;

      console.log('TuTarjetaMx email');

      $scope.lettersForNames = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ-\s]+$/;
      $scope.onlyNumbers = /^\d+$/;
      $scope.emailExpression = /^\w+([\.\+-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;

      $scope.f = {};

      $scope.change = function(prod) {
        $scope.f.products = $scope.f.products ? $scope.f.products : [];
        var index = $scope.f.products.indexOf(prod);
        if (index >= 0) {
          $scope.f.products.splice(index, 1);
        } else {
          $scope.f.products.push(prod);
        }
        console.log($scope.f.products);
      };

      $scope.sendEmail = function(isValid) {
        // Formulario válido
        if (isValid && !$scope.sending) {
          $scope.sending = true;
          $http.post('/email', $scope.f).then(
            res => {
              $scope.sending = false;
              window.location.reload();
            },
            err => {
              $scope.sending = false;
              console.error(err);
            }
          );
        } else {
          // Formulario no válido
          console.log('No es válido');
        }
      };
    }
  ])
  .controller('results', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;
      const stateIds = [6, 24, 25, 28]; // Chihuahua, Sonora, Veracruz, Tabasco
      const professionIds = [3, 4]; // Desempleado, estudiante
      const incomeLess7k = [1]; // >=7k
      const incomeLess15k = [1, 2]; // >=15k
      const incomeLess25k = [1, 2, 3]; // >=25k
      const incomeMore25k = [4, 5, 6]; // <25k
      const moneyManId = 1;
      const crediteaId = 2;
      const kueskiId = 3;
      const coruId = 4;
      const bbva = 8;
      const albo = 6;

      console.log('TuTarjetaMx results');

      var debit = sessionStorage.getItem('debit');
      var paymentUpToDate = sessionStorage.getItem('paymentUpToDate');
      var yearBirth = parseInt(sessionStorage.getItem('yearBirth'));
      var professionId = parseInt(sessionStorage.getItem('professionId'));
      var stateId = parseInt(sessionStorage.getItem('stateId'));
      var incomeId = parseInt(sessionStorage.getItem('incomeId'));

      var showCards = [];

      // if (
      //   yearBirth >= 1970 &&
      //   yearBirth <= 1995 &&
      //   !incomeLess7k.includes(incomeId) &&
      //   !professionIds.includes(professionId) &&
      //   debit == 'true' &&
      //   paymentUpToDate != 'false'
      // ) {
      //   showCards.push(moneyManId);
      // }
      //
      // if (
      //   yearBirth >= 1970 &&
      //   yearBirth <= 1992 &&
      //   !incomeLess15k.includes(incomeId) &&
      //   !stateIds.includes(stateId) &&
      //   !professionIds.includes(professionId) &&
      //   debit == 'true' &&
      //   paymentUpToDate != 'false'
      // ) {
      //   showCards.push(crediteaId);
      // }
      //
      // if (!incomeLess15k.includes(incomeId)) {
      //   showCards.push(coruId);
      // }
      //
      // if (!showCards.length) {
      //   showCards.push(kueskiId);
      // }

      $http.get('/card?active=true&sort=order').then(
        res => {
          $scope.cards = res.data;
          if (incomeLess25k.includes(incomeId)) {
            $scope.cards = $scope.cards.filter(c => {
              return c.id != bbva;
            });
          } else {
            $scope.cards = $scope.cards.filter(c => {
              return c.id != albo;
            });
          }
          $scope.cards.forEach(c => {
            c.description = c.description.split(String.fromCharCode(10));
          });
        },
        err => {
          console.error(err);
        }
      );
    }
  ])
  .controller('pages', [
    '$scope',
    '$http',
    '$sce',
    '$location',
    function($scope, $http, $sce, $location) {
      var vm = this;
      var path = $location.absUrl().split('/')[3];
      var idPage = 0;

      console.log('TuTarjetaMx pages');

      switch (path) {
        case 'about':
          idPage = 1;
          break;
        case 'loan':
          idPage = 2;
          break;
        case 'healers':
          idPage = 3;
          break;
        case 'terms':
          idPage = 4;
          break;
        case 'privacy':
          idPage = 5;
          break;
        case 'faq':
          idPage = 6;
          break;
      }

      $http.get('/page/' + idPage).then(
        res => {
          $scope.page = res.data;
          $scope.page.htmlContent = $sce.trustAsHtml($scope.page.htmlContent);
        },
        err => {
          console.error(err);
        }
      );
    }
  ]);