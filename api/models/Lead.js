/**
 * Lead.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 * 
 */
const uuidv4 = require('uuid/v4');

module.exports = {

  attributes: {

    id: {
      type: 'string',
      unique: true,
      autoIncrement: true
    },
    name: {
      type: 'string',
      allowNull: true
    },
    paternalSurname: {
      type: 'string',
      allowNull: true
    },
    maternalSurname: {
      type: 'string',
      allowNull: true
    },
    email: {
      type: 'string',
      required: true
    },
    phone: {
      type: 'string',
      required: true
    },
    dayBirth: {
      type: 'string',
      allowNull: true
    },
    monthBirth: {
      type: 'string',
      allowNull: true
    },
    yearBirth: {
      type: 'string',
      allowNull: true
    },
    curp: {
      type: 'string',
      allowNull: true
    },
    state: {
      model: 'state',
      columnName: 'stateId'
    },
    gender: {
      model: 'gender',
      columnName: 'genderId'
    },
    civilState: {
      model: 'civilState',
      columnName: 'civilStateId'
    }

  },

  beforeCreate: function (valuesToSet, proceed) {
    valuesToSet.id = uuidv4();
    return proceed();
  }

};
